﻿using System.Collections.Generic;
using NGDP.Utilities;

namespace NGDP.NGDP
{
    public class Root
    {
        public Dictionary<ulong, Record> Records { get; } = new Dictionary<ulong, Record>();

        public bool TryGetByHash(ulong hash, out Record record) => Records.TryGetValue(hash, out record);

        public bool Loaded { get; private set; } = false;

        public void FromStream(string host, string queryString)
        {
            using (var blte = new BLTE(host))
            {
                blte.Send(queryString);
                if (blte.Failed)
                    return;

                using (var fileReader = new EndianBinaryReader(EndianBitConverter.Little, blte.ReadToEnd()))
                {
                    while (fileReader.BaseStream.Position < fileReader.BaseStream.Length)
                    {
                        var recordCount = fileReader.ReadInt32();
                        var contentFlags = fileReader.ReadInt32();
                        var localeFlags = fileReader.ReadInt32();

                        var records = new Record[recordCount];
                        var fileDataIndex = 0;

                        for (var i = 0; i < recordCount; ++i)
                        {
                            // ReSharper disable once UseObjectOrCollectionInitializer
                            records[i] = new Record();
                            records[i].LocaleFlags = localeFlags;
                            records[i].ContentFlags = contentFlags;

                            records[i].FileDataID = fileDataIndex + fileReader.ReadInt32();
                            fileDataIndex = records[i].FileDataID + 1;
                        }

                        for (var i = 0; i < recordCount; ++i)
                        {
                            records[i].MD5 = fileReader.ReadBytes(16);
                            records[i].Hash = fileReader.ReadUInt64();

                            Records[records[i].Hash] = records[i];
                        }
                    }
                }
            }

            Loaded = true;
        }

        public class Record
        {
            public byte[] MD5 { get; set; }
            public ulong Hash { get; set; }
            public int FileDataID { get; set; }
            public int ContentFlags { get; set; }
            public int LocaleFlags { get; set; }
        }
    }
}
