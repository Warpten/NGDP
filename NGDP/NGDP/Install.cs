﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NGDP.Utilities;

namespace NGDP.NGDP
{
    public class Install
    {
        public bool Loaded { get; private set; }

        public class Record
        {
            public ulong NameHash { get; set; }
            public byte[] MD5 { get; set; }
            public int Size { get; set; }

            // public List<InstallTag> Tags { get; set; }
        }

        // public class InstallTag
        // {
        //     public string Name { get; set; }
        //     public short Type { get; set; }
        //     public BitArray Bits { get; set; }
        // }

        public List<Record> Records { get; } = new List<Record>();

        public void FromNetworkResource(string host, string queryString)
        {
            using (var blte = new BLTE(host))
            {
                blte.Send(queryString);

                if (!blte.Failed)
                    FromStream(blte);
                else
                    Loaded = false;
            }
        }

        public void FromStream(Stream stream)
        {
            using (var reader = new EndianBinaryReader(EndianBitConverter.Big, stream))
            {
                reader.ReadInt32(); // Skip signature and two unknown bytes

                var tagCount = reader.ReadInt16();
                var entryCount = reader.ReadInt32();

                var numMaskBytes = entryCount / 8 + (entryCount % 8 > 0 ? 1 : 0);

                for (var i = 0; i < tagCount; ++i)
                {
                    reader.ReadCString(); // Tag name
                    reader.ReadBytes(numMaskBytes + 2);
                }

                for (var i = 0; i < entryCount; ++i)
                {
                    // ReSharper disable once UseObjectOrCollectionInitializer
                    var record = new Record();

                    record.NameHash = JenkinsHashing.Instance.ComputeHash(reader.ReadCString());
                    record.MD5 = reader.ReadBytes(16);
                    record.Size = reader.ReadInt32();

                    Records.Add(record);
                }
            }

            Loaded = true;
        }

        public IEnumerable<Record> GetEntriesByName(string fileName) =>
            Records.Where(entry => entry.NameHash == JenkinsHashing.Instance.ComputeHash(fileName));

        public bool HasFile(string fileName) =>
            Records.Any(r => r.NameHash == JenkinsHashing.Instance.ComputeHash(fileName));
    }
}
