﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NGDP.NGDP;
using NGDP.Patch;
using NGDP.Utilities;

namespace NGDP.Local
{
    public class BuildInfo
    {
        public Encoding Encoding { get; } = new Encoding();
        public Root Root { get; } = new Root();
        public IndexStore Indices { get; } = new IndexStore();
        public Install Install { get; } = new Install();

        public BuildConfiguration BuildConfiguration { get; set; }
        public ContentConfiguration ContentConfiguration { get; set; }
        public CDNs.Record ServerInfo { get; set; }

        public string VersionName { get; set; }

        public event Action OnReady;
        public bool Ready { get; private set; }
        public bool Loading { get; private set; }

        public async void Prepare()
        {
            Loading = true;

            var encodingTask = Task.Run(() =>
            {
                Console.WriteLine($"[{VersionName}] Downloading encoding ...");
                Encoding.FromNetworkResource(ServerInfo.Hosts[0],
                    $"/{ServerInfo.Path}/data/{BuildConfiguration.Encoding[1][0]:x2}/{BuildConfiguration.Encoding[1][1]:x2}/{BuildConfiguration.Encoding[1].ToHexString()}");
                Console.WriteLine($"[{VersionName}] Encoding downloaded.");
            });

            var rootTask = Task.Run(() =>
            {
                while (true)
                {
                    Thread.Sleep(1000);

                    Encoding.Entry rootEncodingEntry;
                    if (!Encoding.Records.TryGetValue(BuildConfiguration.Root, out rootEncodingEntry))
                        continue;

                    Console.WriteLine($"[{VersionName}] Downloading root ...");
                    Root.FromStream(ServerInfo.Hosts[0],
                        $"/{ServerInfo.Path}/data/{rootEncodingEntry.Key[0]:x2}/{rootEncodingEntry.Key[1]:x2}/{rootEncodingEntry.Key.ToHexString()}");
                    Console.WriteLine($"[{VersionName}] Root downloaded.");
                    break;
                }
            });

            var indicesTask = Task.Run(() =>
            {
                Console.WriteLine($"[{VersionName}] Downloading indices ...");
                Indices.FromStream(ServerInfo.Hosts[0], ContentConfiguration.Archives);
                Console.WriteLine($"[{VersionName}] Indices downloaded.");
            });

            await Task.WhenAll(encodingTask, rootTask, indicesTask);

            await Task.Run(() =>
            {
                Action<string, string, byte[]> installLoader = (host, path, hash) =>
                {
                    Install.FromNetworkResource(host,
                        $"/{path}/data/{hash[0]:x2}/{hash[1]:x2}/{hash.ToHexString()}");
                };

                installLoader(ServerInfo.Hosts[0], ServerInfo.Path, BuildConfiguration.Install);
                if (!Install.Loaded)
                {
                    Encoding.Entry encodingEntry;
                    if (Encoding.Records.TryGetValue(BuildConfiguration.Install, out encodingEntry))
                        installLoader(ServerInfo.Hosts[0], ServerInfo.Path, encodingEntry.Key);
                }

                if (Install.Loaded)
                    Console.WriteLine($"[{VersionName}] Install downloaded.");
            });

            Ready = true;
            Loading = false;
            OnReady?.Invoke();
        }

        public void Clear()
        {
            Ready = false;
            Encoding.Records.Clear();
            Root.Records.Clear();
            Indices.Records.Clear();
        }

        public IndexStore.Record GetEntry(string fileName)
        {
            if (!Ready)
                return null;

            Encoding.Entry encodingRecord;

            Root.Record rootRecord;
            if (Root.TryGetByHash(JenkinsHashing.Instance.ComputeHash(fileName), out rootRecord))
            {
                if (!Encoding.Records.TryGetValue(rootRecord.MD5, out encodingRecord))
                    return null;
            }
            else if (Install.HasFile(fileName))
            {
                var installEntry = Install.GetEntriesByName(fileName).First();
                if (!Encoding.Records.TryGetValue(installEntry.MD5, out encodingRecord))
                    return null;
            }
            else
                return null;

            IndexStore.Record indexEntry;
            if (!Indices.Records.TryGetValue(encodingRecord.Key, out indexEntry))
            {
                return new IndexStore.Record
                {
                    Offset = 0,
                    Size = -1,
                    ArchiveIndex = -1, // Use as a marker for whole-size archives
                    Hash = encodingRecord.Key
                };
            }

            return indexEntry;
        }
    }
}
