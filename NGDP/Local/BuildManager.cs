﻿using System.Collections.Generic;
using NGDP.Utilities;

namespace NGDP.Local
{
    public static class BuildManager
    {
        public static Dictionary<ulong, BuildInfo> Builds { get; } = new Dictionary<ulong, BuildInfo>();

        public static bool IsBuildKnown(string buildName) => Builds.ContainsKey(JenkinsHashing.Instance.ComputeHash(buildName));
        public static void AddBuild(string buildName, BuildInfo build) => Builds.Add(JenkinsHashing.Instance.ComputeHash(buildName), build);
        public static BuildInfo GetBuild(string buildName) => Builds[JenkinsHashing.Instance.ComputeHash(buildName)];
    }
}
