﻿using System.Threading.Tasks;
using Meebey.SmartIrc4net;
using NGDP.Local;
using NGDP.Utilities;
using System;
using System.IO;

namespace NGDP.Commands
{
    public static class Handlers
    {
        [CommandHandler("$status", "")]
        public static void HandleStatus(IrcClient client, IrcMessageData messageData)
        {
            var channelUser = (NonRfcChannelUser)client.GetChannelUser(messageData.Channel, messageData.Nick);
            if (channelUser == null || !(channelUser.IsHalfop || channelUser.IsOp || channelUser.IsChannelAdmin || channelUser.IsOwner))
                return;

            client.SendReply(messageData, Program.Running ? "Running." : "Why am I even here?");
        }

        [CommandHandler("$downloadfile", "$downloadfile <build name string> <filePath>")]
        public static void HandleDownloadFile(IrcClient client, IrcMessageData messageData)
        {
            var channelUser = (NonRfcChannelUser)client.GetChannelUser(messageData.Channel, messageData.Nick);
            if (channelUser == null || !(channelUser.IsHalfop || channelUser.IsOp || channelUser.IsChannelAdmin || channelUser.IsOwner))
                return;

            if (messageData.MessageArray.Length != 3)
            {
                client.SendReply(messageData, Dispatcher.GetUsage(messageData));
                return;
            }

            if (!BuildManager.IsBuildKnown(messageData.MessageArray[1]))
            {
                client.SendReply(messageData, "Unknown build.");
                return;
            }

            CheckFileExists(messageData.MessageArray[1],
                buildInfo =>
                {
                    var indexEntry = buildInfo.GetEntry(messageData.MessageArray[2]);
                    if (indexEntry == null)
                    {
                        client.SendReply(messageData, $"{messageData.Nick}: File does not exist.");
                        return;
                    }

                    // Give out link
                    var response = indexEntry.ArchiveIndex != -1 ?
                        $"http://ngdp.herokuapp.com/{buildInfo.VersionName}/{buildInfo.Indices.Archives[indexEntry.ArchiveIndex].ToHexString()}/{indexEntry.Offset}/{indexEntry.Offset + indexEntry.Size - 1}" :
                        $"http://ngdp.herokuapp.com/{buildInfo.VersionName}/{indexEntry.Hash.ToHexString()}";

                    client.SendReply(messageData,
                        $"{messageData.Nick}: {response}/{Path.GetFileName(messageData.MessageArray[2].Replace('\\', '/'))}");
                },
                buildInfo =>
                {
                    client.SendReply(messageData,
                        $"{messageData.Nick}: Loading {buildInfo.VersionName}. Please re-query in a while.");
                },
                () =>
                {
                    var buildInfo = BuildManager.GetBuild(messageData.MessageArray[1]);
                    if (buildInfo.Loading)
                    {
                        client.SendReply(messageData,
                            $"{messageData.Nick}: Build {buildInfo.VersionName} is currently loading.");
                        return;
                    }

                    client.SendReply(messageData,
                        $"{messageData.Nick}: Build {buildInfo.VersionName} has been loaded, and will unload in 30 minutes.");

                    if (!buildInfo.Install.Loaded)
                        client.SendReply(messageData, $"{messageData.Nick}: Install file could not be downloaded, expect erroneous results.");

                    Task.Delay(30 * 60 * 1000).ContinueWith(t =>
                    {
                        buildInfo.Clear();

                        client.SendReply(messageData, $"Data from build {buildInfo.VersionName} has been unloaded.");
                    });
                });
        }

        [CommandHandler("$checkfile", "$checkfile <build name string> <filePath>")]
        public static void HandleCheckFile(IrcClient client, IrcMessageData messageData)
        {
            var channelUser = (NonRfcChannelUser)client.GetChannelUser(messageData.Channel, messageData.Nick);
            if (channelUser == null || !(channelUser.IsHalfop || channelUser.IsOp || channelUser.IsChannelAdmin || channelUser.IsOwner))
                return;

            if (messageData.MessageArray.Length != 3)
            {
                client.SendReply(messageData, Dispatcher.GetUsage(messageData));
                return;
            }

            if (!BuildManager.IsBuildKnown(messageData.MessageArray[1]))
            {
                client.SendReply(messageData, "Unknown build.");
                return;
            }

            CheckFileExists(messageData.MessageArray[1],
                // Success handler (build is ready)
                buildInfo =>
                {
                    var indexEntry = buildInfo.GetEntry(messageData.MessageArray[2]);
                    if (indexEntry == null)
                    {
                        client.SendReply(messageData, $"{messageData.Nick}: File does not exist.");
                        return;
                    }

                    client.SendReply(messageData, $"{messageData.Nick}: File exists.");
                },
                // Fail handler (build not ready)
                buildInfo =>
                {
                    client.SendReply(messageData,
                        $"{messageData.Nick}: Loading {buildInfo.VersionName}. Please re-query in a while.");
                },
                // Fail handler (execute when build is ready, delayed)
                () =>
                {
                    var buildInfo = BuildManager.GetBuild(messageData.MessageArray[1]);
                    if (buildInfo.Loading)
                    {
                        client.SendReply(messageData,
                            $"{messageData.Nick}: Build {buildInfo.VersionName} is currently loading.");
                        return;
                    }

                    client.SendReply(messageData,
                        $"{messageData.Nick}: Build {buildInfo.VersionName} has been loaded, and will unload in 30 minutes.");

                    if (!buildInfo.Install.Loaded)
                        client.SendReply(messageData, $"{messageData.Nick}: Install file could not be downloaded, expect erroneous results.");

                    Task.Delay(30 * 60 * 1000).ContinueWith(t =>
                    {
                        buildInfo.Clear();

                        client.SendReply(messageData, $"Data from build {buildInfo.VersionName} has been unloaded.");
                    });
                });
        }

        [CommandHandler("$listbuilds", "")]
        public static void HandleListBuilds(IrcClient client, IrcMessageData messageData)
        {
            var channelUser = (NonRfcChannelUser)client.GetChannelUser(messageData.Channel, messageData.Nick);
            if (channelUser == null || !(channelUser.IsHalfop || channelUser.IsOp || channelUser.IsChannelAdmin || channelUser.IsOwner))
                return;

            foreach (var kv in BuildManager.Builds)
                client.SendReply(messageData, kv.Value.Ready ? $"* {kv.Key} [LOADED]" : $"* {kv.Key}");
        }

        private static void CheckFileExists(string buildName,
            Action<BuildInfo> successHandler, Action<BuildInfo> failHandler, Action onReady)
        {
            var buildInfo = BuildManager.GetBuild(buildName);

            if (!buildInfo.Ready)
            {
                failHandler(buildInfo);
                buildInfo.OnReady += onReady;
                if (!buildInfo.Loading)
                    buildInfo.Prepare();
            }
            else
                successHandler(buildInfo);
        }
    }
}
