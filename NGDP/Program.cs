﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Meebey.SmartIrc4net;
using NGDP.Commands;
using NGDP.Local;
using NGDP.Network;

namespace NGDP
{
    internal static class Program
    {
        private static bool _running = true;
        private static IrcClient IRC { get; set; }
        public static List<Channel> Channels { get; } = new List<Channel>();
        public static List<BuildInfo> Builds { get; } = new List<BuildInfo>();

        public static bool SendConfigLinks { get; } = false;
        public static bool Running => _running;

        private static HttpServer HttpServer;

        static void Main(string[] args)
        {
            #region Create channel monitors
            Channels.Clear();
            Channels.Add(new Channel
            {
                ChannelName = "wow",
                DisplayName = "Retail",
                DownloadBinaries = false
            });

            Channels.Add(new Channel
            {
                ChannelName = "wowt",
                DisplayName = "PTR",
                DownloadBinaries = false
            });

            Channels.Add(new Channel
            {
                ChannelName = "wow_beta",
                DisplayName = "Beta",
                DownloadBinaries = false
            });
            #endregion

            foreach (var channel in Channels)
                channel.MessageEvent += (t, c, m) => {
                    IRC?.SendMessage(t, c, m);
                    Console.WriteLine($"[{channel.ChannelName}] {m}");
                };

            Console.CancelKeyPress += (s, ea) => {
                Console.WriteLine("Aborting ...");
                _running = false;
                IRC.Disconnect();
                HttpServer?.Stop();
            };

            IRC = new IrcClient {
                SupportNonRfc = true,
                ActiveChannelSyncing = true
            };

            IRC.OnConnected += (sender, eventArgs) => {
                Connect();
            };

            IRC.OnChannelMessage += (sender, eventArgs) => {
                Dispatcher.Dispatch(eventArgs.Data, IRC);
            };

            HttpServer = new HttpServer(80);

            IRC.Connect(@host, @port);
            IRC.Login(@nick, @realName, 0, @serverUser, @serverPass);
            IRC.RfcJoin(@channel);
            IRC.Listen();

            while (_running)
                Thread.Sleep(10000);// Time to fuck with the CPU
            // ReSharper disable once FunctionNeverReturns
        }

        private static void Connect()
        {
            Task.Factory.StartNew(() =>
            {
                while (_running)
                {
                    Console.WriteLine("Updating channels ...");
                    foreach (var channel in Channels)
                        channel.Update();
                    Thread.Sleep(10000);
                }
            });
        }
    }
}
