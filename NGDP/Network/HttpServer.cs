﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using NGDP.Local;
using NGDP.NGDP;

namespace NGDP.Network
{
    public class HttpServer
    {
        private Thread _serverThread;
        private HttpListener _listener;

        public int Port { get; private set; }

        /// <summary>
        /// Construct server with given port.
        /// </summary>
        /// <param name="port">Port of the server.</param>
        public HttpServer(int port)
        {
            Initialize(port);
        }

        /// <summary>
        /// Construct server with suitable port.
        /// </summary>
        public HttpServer()
        {
            // get an empty port
            var l = new TcpListener(IPAddress.Loopback, 0);
            l.Start();
            var port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.Stop();
            Initialize(port);
        }

        /// <summary>
        /// Stop server and dispose all functions.
        /// </summary>
        public void Stop()
        {
            _serverThread.Abort();
            _listener.Stop();
        }

        private void Listen()
        {
            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PORT")))
                Port = int.Parse(Environment.GetEnvironmentVariable("PORT"));

            Console.WriteLine("Listening on port {0}", Port);

            _listener = new HttpListener();
            _listener.Prefixes.Add("http://*:" + Port + "/");
            _listener.Start();

            while (true)
            {
                try
                {
                    var context = _listener.GetContext();
                    Process(context);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        private static void Process(HttpListenerContext context)
        {
            var tokens = context.Request.Url.AbsolutePath.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

            if (tokens.Length == 3 || tokens.Length == 5)
            {
                var buildName = tokens[0];
                var archiveName = tokens[1];
                var rangeStart = tokens.Length == 5 ? tokens[2] : string.Empty;
                var rangeEnd = tokens.Length == 5 ? tokens[3] : string.Empty;

                if (!BuildManager.IsBuildKnown(buildName))
                    HandleUnknownBuild(context);
                else
                {
                    var buildInfo = BuildManager.GetBuild(buildName);

                    try
                    {
                        using (var blte = new BLTE(buildInfo.ServerInfo.Hosts[0]))
                        {
                            if (!string.IsNullOrEmpty(rangeStart) && !string.IsNullOrEmpty(rangeEnd))
                                blte.AddHeader("Range", $"bytes={rangeStart}-{rangeEnd}");

                            blte.Send(
                                $"/{buildInfo.ServerInfo.Path}/data/{archiveName.Substring(0, 2)}/{archiveName.Substring(2, 2)}/{archiveName}");

                            if (!blte.Failed)
                            {
                                context.Response.ContentType = "application/octet-stream";
                                context.Response.ContentLength64 = blte.DecompressedLength;
                                context.Response.AddHeader("Date", blte.ResponseHeaders.Get("Date"));
                                context.Response.AddHeader("ETag", blte.ResponseHeaders.Get("ETag"));
                                context.Response.AddHeader("Last-Modified", DateTime.Now.ToString("r"));
                                context.Response.AddHeader("Connection", "Keep-Alive");

                                context.Response.StatusCode = (int)HttpStatusCode.OK;

                                blte.PipeTo(context.Response.OutputStream);
                            }
                            else
                            {
                                throw new InvalidOperationException("Unable to read BLTE stream from Blizzard.");
                            }
                        }
                    }
                    catch (InvalidOperationException ioe)
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.ContentLength64 = ioe.Message.Length;
                        context.Response.AddHeader("Connection", "close");

                        context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                        var writer = new StreamWriter(context.Response.OutputStream);
                        writer.Write(ioe);

                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        context.Response.OutputStream.Flush();
                        context.Response.OutputStream.Close();
                    }
                    catch (Exception e)
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                        var writer = new StreamWriter(context.Response.OutputStream);
                        writer.Write(e);

                        context.Response.OutputStream.Flush();
                        context.Response.OutputStream.Close();
                    }
                }
            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.ContentLength64 = 0;
                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));
                context.Response.AddHeader("Last-Modified", DateTime.Now.ToString("r"));
                context.Response.AddHeader("Connection", "close");

                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                var writer = new StreamWriter(context.Response.OutputStream);
                writer.WriteLine("Invalid request URL.");
            }

            context.Response.OutputStream.Flush();
            context.Response.OutputStream.Close();
        }

        private static void HandleUnknownBuild(HttpListenerContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.ContentLength64 = 0;
            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));
            context.Response.AddHeader("Last-Modified", DateTime.Now.ToString("r"));
            context.Response.AddHeader("Connection", "close");

            context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            var writer = new StreamWriter(context.Response.OutputStream);
            writer.WriteLine("Unknown build.");
        }

        private void Initialize(int port)
        {
            Port = port;
            _serverThread = new Thread(Listen);
            _serverThread.Start();
        }


    }
}
