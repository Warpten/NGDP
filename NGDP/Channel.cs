﻿using System;
using System.Threading;
using Meebey.SmartIrc4net;
using NGDP.Local;
using NGDP.NGDP;
using NGDP.Patch;

namespace NGDP
{
    public class Channel
    {
        public string ChannelName { get; set; }
        public string DisplayName { get; set; }
        public bool DownloadBinaries { get; set; }

        public event Action<SendType, string, string> MessageEvent;

        public void Update()
        {
            using (var versions = new Versions(ChannelName))
            using (var cdns = new CDNs(ChannelName))
            {
                // Walk through versions
                foreach (var versionInfo in versions.Records)
                {
                    // Get CDN data.
                    CDNs.Record serverInfo;
                    if (!cdns.Records.TryGetValue(versionInfo.Value.Region, out serverInfo))
                        serverInfo = cdns.Records["eu"];

                    var versionName = versionInfo.Value.GetName(DisplayName);

                    if (BuildManager.IsBuildKnown(versionName))
                        continue;

                    var buildInfo = new BuildInfo {
                        VersionName = versionName,
                        ServerInfo = serverInfo
                    };

                    BuildManager.AddBuild(versionName, buildInfo);

                    MessageEvent?.Invoke(SendType.Message, "#trinitycore", $"Build {versionName} was deployed.");

                    // Get build info
                    buildInfo.BuildConfiguration = new BuildConfiguration(serverInfo.Hosts[0], versionInfo.Value.BuildConfig);
                    buildInfo.ContentConfiguration = new ContentConfiguration(serverInfo.Hosts[0], versionInfo.Value.CDNConfig);

                    /* if (Program.SendConfigLinks)
                    {
                        MessageEvent?.Invoke(SendType.Message, "#trinitycore", $"Build config: {buildConfig.URL}");
                        MessageEvent?.Invoke(SendType.Message, "#trinitycore", $"CDN Config: {cdnConfig.URL}");
                    }*/

                    /*Task.Factory.StartNew(() =>
                    {
                        buildInfo.AddFile("Wow.exe", "Wow.exe");
                        buildInfo.AddFile("Wow-64.exe", "Wow-64.exe");
                        buildInfo.AddFile("WowT.exe", "WowT.exe");
                        buildInfo.AddFile("WowT-64.exe", "WowT-64.exe");
                        buildInfo.AddFile("WowB.exe", "WowB.exe");
                        buildInfo.AddFile("WowB-64.exe", "WowB-64.exe");
                        buildInfo.AddFile("WowGM.exe", "WowGM.exe");
                        buildInfo.AddFile("WowGM-64.exe", "WowGM-64.exe");
                        buildInfo.AddFile(
                            @"World of Warcraft.app\Contents\MacOS\World of Warcraft.dSYM\Contents\Resources\DWARF\World of Warcraft",
                            "World of Warcraft");

                        buildInfo.OnFileDownloadResult += (result, file) =>
                        {
                            if (result != DownloadResult.Success)
                                return;

                            MessageEvent?.Invoke(SendType.Message,
                                "#trinitycore",
                                $"{file.LocalFilename.Split('\\').Last()} downloaded for build {buildInfo.DirectoryName}.");
                        };
                        buildInfo.Download(this, serverInfo);
                    });*/
                }
            }

            // Sleep one second to make sure every message goes through.
            Thread.Sleep(500);
        }
    }
}
